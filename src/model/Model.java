/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import data.ChangeDisperser;
import data.AdminReport;
import data.CocaCola;
import data.Crisps;
import data.ECoinTypes;
import data.ESnackTypes;
import data.Eugenia;
import data.ISnack;
import data.MarsBar;
import data.SyncUpData;
import data.Water;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.text.DecimalFormat;
import java.util.HashMap;

/**
 *
 * @author home
 */
public class Model {
    final PropertyChangeSupport     propChangeSupp      = new PropertyChangeSupport(this);
    HashMap<ESnackTypes, ISnack>    snacksMap           = new HashMap<>();// snack id   -   snack object
    HashMap<ESnackTypes, Double>    profitPerSnackMap   = new HashMap<>();// snack id   -   profit
    ChangeDisperser                 changeDisperser     = new ChangeDisperser();
    Double                          dInsertedCoins      = 0.0;
    Double                          dCashInAmount       = 0.0;
    Double                          dBuyingPrice        = 0.45;
    DecimalFormat                   decimalForm         = new DecimalFormat("#.########");

    final String                    user                = "10976";
    final String                    pass                = "1234";
   
    public synchronized void initialize(){
        dInsertedCoins      = 0.0;
        dCashInAmount       = 0.0;    
        
        try{
            initializeSnacks();
            initializeProfitPerSnack();
            changeDisperser.initialize();
        }catch(Exception ex){
            throw ex;
        }
    }
    
    public synchronized void transferInsertedChangeToMachine(ECoinTypes type, int nValue) {
        int lastCountValue = changeDisperser.getChangeCountByType(type);
        this.changeDisperser.setChangeCountByType(type, nValue + lastCountValue);
    }
    
    public synchronized void login(String user, String pass){
        if ((this.user.equals(user)) && (this.pass.equals(pass))){
            AdminReport report = new AdminReport();
            Double dTotalChange = changeDisperser.getTotalChange();
            
            report.dTotalMoney = dTotalChange;
            report.dTotalMoney = Double.valueOf(decimalForm.format(report.dTotalMoney));
            report.dProfit =  calculateTotalProfits();
            report.dProfit = Double.valueOf(decimalForm.format(report.dProfit));
            report.dLosses = 0.0;
            
            this.propChangeSupp.firePropertyChange("Logged", null, report);
        }else{
            this.propChangeSupp.firePropertyChange("Error", null, "Invalid User or Password!");
        }
    }
        
    public synchronized void showPopUpErrorMessage(String szMessage){
        this.propChangeSupp.firePropertyChange("Error", null, szMessage);
    }
    
    public synchronized void syncByType(ESnackTypes type){
        SyncUpData data = new SyncUpData();
        
        data.type = type;
        data.dCashIn = this.dCashInAmount;
        data.dInsertedCoins = this.dInsertedCoins;
        data.dPrice = this.getSnackPriceByType(type);
        data.nQuantity = this.getSnackQuantityByType(type);
        
        this.propChangeSupp.firePropertyChange("SyncUp", null, data);
    }
    
    public synchronized Double getSnackPriceByType(ESnackTypes type){
        return snacksMap.get(type).getdPrice();
    }
    
    public synchronized int getSnackQuantityByType(ESnackTypes type){
        return snacksMap.get(type).getnQuantity();
    }
    
    public synchronized void setSnackQuantityByType(ESnackTypes type, int nQuantity){
        int nOldQuantity = snacksMap.get(type).getnQuantity();
        String szEventName;
        
        switch(type){
            case E_CRISPS:
                szEventName = "CrispsQuantity";
                break;
            case E_MARS:
                szEventName = "MarsQuantity";
                break;
            case E_COCA_COLA:
                szEventName = "CokeQuantity";
                break;
            case E_EUGENIA:
                szEventName = "EugeniaQuantity";
                break;
            case E_WATER:
                szEventName = "WaterQuantity";
                break;
            default:
                throw new AssertionError(type.name());
        }
        
        this.propChangeSupp.firePropertyChange(szEventName, nOldQuantity, nQuantity);
        
        snacksMap.get(type).setnQuantity(nQuantity);
    }
    
    public synchronized Double getCoinValueByType(ECoinTypes type){
        return changeDisperser.getCoinValueByType(type);
    }
        
    public synchronized int getChangeCountByType(ECoinTypes type){
        return changeDisperser.getChangeCountByType(type);
    }
    
    public synchronized void setChangeCountByType(ECoinTypes type, int nNewValue){
        changeDisperser.setChangeCountByType(type,nNewValue);      
    }
    
    public synchronized Double getdInsertedCoins() {
        return dInsertedCoins;
    }

    public synchronized void setdInsertedCoins(Double dInsertedCoins) {
        this.propChangeSupp.firePropertyChange("InsertedCoin", this.dInsertedCoins, dInsertedCoins);
        this.dInsertedCoins = dInsertedCoins;
    }

    public synchronized Double getdCashInAmount() {
        return dCashInAmount;
    }

    public synchronized void setdCashInAmount(Double dCashInAmount) {
        try{
            changeDisperser.setOptimalChange(dCashInAmount);
        } catch (Exception ex) {
            this.propChangeSupp.firePropertyChange("Error", null, ex.getMessage());
            dCashInAmount = this.dInsertedCoins;
        }
        
        dCashInAmount += this.dCashInAmount;
        
        this.propChangeSupp.firePropertyChange("PayForSnack", this.dCashInAmount, dCashInAmount);
        this.dCashInAmount = dCashInAmount;
    }

    public synchronized void resetCashInAmount(){
        this.propChangeSupp.firePropertyChange("CashInAmount", this.dCashInAmount, 0.0);
        this.dCashInAmount = 0.0;
    }
    
    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        propChangeSupp.addPropertyChangeListener(listener);
    }
    
    private synchronized void initializeSnacks(){
        ISnack crispsSnack = SnacksFactory.GetSnack(ESnackTypes.E_CRISPS);
        ISnack marsSnack = SnacksFactory.GetSnack(ESnackTypes.E_MARS);
        ISnack cokeSnack = SnacksFactory.GetSnack(ESnackTypes.E_COCA_COLA);
        ISnack eugeniaSnack = SnacksFactory.GetSnack(ESnackTypes.E_EUGENIA);
        ISnack waterSnack = SnacksFactory.GetSnack(ESnackTypes.E_WATER);

        snacksMap.put(ESnackTypes.E_CRISPS, crispsSnack);
        snacksMap.put(ESnackTypes.E_MARS, marsSnack);
        snacksMap.put(ESnackTypes.E_COCA_COLA, cokeSnack);
        snacksMap.put(ESnackTypes.E_EUGENIA, eugeniaSnack);
        snacksMap.put(ESnackTypes.E_WATER, waterSnack);
    }
    
    private synchronized void initializeProfitPerSnack(){
        for(int i = 0; i < snacksMap.size(); i++){
            ISnack      snack;
            ESnackTypes type;
            Double      dProfit;
            
            type = getSnackTypeByIndex(i);
            snack = snacksMap.get(type);
            
            dProfit = snack.getdPrice() - dBuyingPrice;
            
            profitPerSnackMap.put(type, Double.valueOf(decimalForm.format(dProfit)));
        }
    }
    
    private synchronized Double calculateTotalProfits(){
        Double dTotalProfits = 0.0;
        
        for(int i = 0; i < snacksMap.size(); i++){
            ISnack      snack;
            ESnackTypes type;
            Double      dSnackProfit;
            
            type = getSnackTypeByIndex(i);
            snack = snacksMap.get(type);
            
            dSnackProfit = profitPerSnackMap.get(type);
            
            dTotalProfits += snack.getSoldQuantity() * dSnackProfit;
        }
        
        return Double.valueOf(decimalForm.format(dTotalProfits));
    }
    
    private synchronized int getIndexBySnackType(ESnackTypes type){
        int index = 0;
        switch(type){
            case E_CRISPS:
                index = 0;
                break;
            case E_MARS:
                index = 1;
                break;
            case E_COCA_COLA:
                index = 2;
                break;
            case E_EUGENIA:
                index = 3;
                break;
            case E_WATER:
                index = 4;
                break;
            default:
                throw new AssertionError(type.name());
        
        }
        return index;
    }
    
    private synchronized ESnackTypes getSnackTypeByIndex(int index){
        ESnackTypes type = ESnackTypes.E_CRISPS;
        
        switch(index){
            case 0:
                type = ESnackTypes.E_CRISPS;
                break;
            case 1:
                type = ESnackTypes.E_MARS;
                break;
            case 2:
                type = ESnackTypes.E_COCA_COLA;
                break;
            case 3:
                type = ESnackTypes.E_EUGENIA;
                break;
            case 4:
                type = ESnackTypes.E_WATER;
                break;
            default:
                throw new AssertionError(type.name());
        
        }
        
        return type;
    }
}
