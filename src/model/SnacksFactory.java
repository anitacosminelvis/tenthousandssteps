package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import data.CocaCola;
import data.Crisps;
import data.ESnackTypes;
import data.Eugenia;
import data.ISnack;
import data.MarsBar;
import data.Water;


/**
 *
 * @author home
 */
public class SnacksFactory {
    static ISnack GetSnack(ESnackTypes eSnackType){
        switch(eSnackType){
            case E_CRISPS:{
                return  Crisps.getInstance();
            }
            case E_MARS:{
                return  MarsBar.getInstance();
            }
            case E_COCA_COLA:{
                return  CocaCola.getInstance();
            }
            case E_EUGENIA:{
                return  Eugenia.getInstance();
            }
            case E_WATER:{
                return  Water.getInstance();
            }
            default: return null;
        }
    }
}
