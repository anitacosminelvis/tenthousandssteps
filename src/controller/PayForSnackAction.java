/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import data.ESnackTypes;
import java.awt.event.ActionEvent;
import java.text.DecimalFormat;
import javax.swing.AbstractAction;
import model.Model;

/**
 *
 * @author home
 */
public class PayForSnackAction extends AbstractAction {
    Model           model;
    ESnackTypes     snackType;
    DecimalFormat   decimalForm = new DecimalFormat("#.########");
    int             selectedQuantity = 1;
    
    public PayForSnackAction(Model model) {
        this.model = model;
    }

    public void setSnackType(ESnackTypes snackType) {
        this.snackType = snackType;
    }
    
    public void setSelectedQuantity(int selectedQuantity) {
        this.selectedQuantity = selectedQuantity;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        PayForSnackWorker payForSnackWorker;
        
        payForSnackWorker = new PayForSnackWorker(model, snackType,selectedQuantity);
        
        payForSnackWorker.execute();
    }
}
