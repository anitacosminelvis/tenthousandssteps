/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import model.Model;

/**
 *
 * @author home
 */
public class LoginAction extends AbstractAction {
    Model           model;
    String          user    = "";
    String          pass    = "";
    
    public LoginAction(Model model) {
        this.model = model;
    }

    public void setUser(String user){
        this.user = user;
    }
    
    public void setPass(String pass){
        this.pass = pass;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        model.login(user, pass);
    }
}
