/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import data.ECoinTypes;
import data.ESnackTypes;
import java.awt.event.ActionEvent;
import java.text.DecimalFormat;
import javax.swing.AbstractAction;
import model.Model;

/**
 *
 * @author home
 */

public class InsertCoinAction extends AbstractAction {
    Model           model;
    ECoinTypes      coinType            = ECoinTypes.E_0_05;
    DecimalFormat   decimalForm         = new DecimalFormat("#.########");
    ESnackTypes     snackType;
    int             selectedQuantity    = 0;

    public InsertCoinAction(Model model) {
        this.model = model;
    }

    public void setCoinType(ECoinTypes coinType) {
        this.coinType = coinType;
    }

    public void setSnackType(ESnackTypes snackType) {
        this.snackType = snackType;
    }
    
    public void setSelectedQuantity(int selectedQuantity) {
        this.selectedQuantity = selectedQuantity;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        Double coinValue = model.getCoinValueByType(coinType);
        Double oldInsertedCoins = model.getdInsertedCoins();
        Double newInsertedCoinsValue = oldInsertedCoins + coinValue;
        Double totalOwedValue = selectedQuantity * model.getSnackPriceByType(snackType);
        
        if (oldInsertedCoins >= totalOwedValue){
            model.showPopUpErrorMessage("No need to insert more coins! If you want more change the amount you want to buy!");
            return;
        }
        
        model.setdInsertedCoins( Double.valueOf(decimalForm.format(newInsertedCoinsValue)) );
        
        model.transferInsertedChangeToMachine(coinType, 1);
    }
}
