/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import data.ESnackTypes;
import java.text.DecimalFormat;
import javax.swing.SwingWorker;
import model.Model;

/**
 *
 * @author home
 */
public class PayForSnackWorker  extends SwingWorker{
    Model           model;
    int             selectedQuantity;
    ESnackTypes     snackType;
    DecimalFormat   decimalForm = new DecimalFormat("#.########");    
    
    PayForSnackWorker(Model model, ESnackTypes snackType, int selectedQuantity){
        this.model = model;
        this.snackType = snackType;
        this.selectedQuantity = selectedQuantity;
    }
    
    @Override
    protected Object doInBackground() throws Exception {
        
        int     snackQuantity = model.getSnackQuantityByType(snackType);
        
        if ((0 == selectedQuantity) && (0 == snackQuantity)){
            model.showPopUpErrorMessage("Out of stock, please select another snack!");
            return null;
        }
        
        if (0 == selectedQuantity){
            model.showPopUpErrorMessage("Please select you snack's quantity!");
            return null;
        }
        
        if (selectedQuantity > snackQuantity){
            model.showPopUpErrorMessage("Selected Quantity is bigger then the actual stock!");
            return null;
        }
        
        double totalOwedValue = selectedQuantity * model.getSnackPriceByType(snackType);
        double insertedCoins = model.getdInsertedCoins();

        if (insertedCoins >= totalOwedValue){
            model.setdCashInAmount(Double.valueOf(decimalForm
                    .format(insertedCoins - totalOwedValue)));
            model.setdInsertedCoins(0.0);
            
            model.setSnackQuantityByType(snackType, snackQuantity - selectedQuantity);
        }else {
            model.showPopUpErrorMessage("Not enough coins inserted!");
        }
        
        return null; 
    }
    
}
