/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import data.ESnackTypes;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import model.Model;

/**
 *
 * @author home
 */
public class CokeSelectionAction  extends AbstractAction {
    Model           model;
    ESnackTypes     type = ESnackTypes.E_COCA_COLA;
    
    public CokeSelectionAction(Model model) {
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        model.syncByType(type);
    }
}
