/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import model.Model;

/**
 *
 * @author home
 */
public class CashInAction extends AbstractAction {
    Model           model;
    
    public CashInAction(Model model) {
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        model.resetCashInAmount();
    }
}
