/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author home
 */
public interface ISnack {
    
    public long getnId();

    public String getSzName();

    public ESnackTypes geteType();

    public Double getdPrice();
    
    public int getSoldQuantity();

    public int getnQuantity();

    public void setnQuantity(int nQuantity);
}
