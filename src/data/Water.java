/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author home
 */
public class Water implements ISnack {
    final long          nId         = 5;
    final String        szName      = "Water";
    final ESnackTypes   eType       = ESnackTypes.E_WATER;
    final int           nInitialNo  = 10; 

    Double              dPrice      = 0.85;
    int                 nQuantity   = 10;

    private Water() {}
    
    public static Water getInstance() {
        return WaterHolder.INSTANCE;
    }
    
    private static class WaterHolder {
        private static final Water INSTANCE = new Water();
    }
    
    @Override
    public int getSoldQuantity() {
        return (nInitialNo - nQuantity);
    }

    @Override
    public int getnQuantity() {
        return nQuantity;
    }

    @Override
    public void setnQuantity(int nQuantity) {
        this.nQuantity = nQuantity;
    }
    
    @Override
    public long getnId() {
        return nId;
    }

    @Override
    public String getSzName() {
        return szName;
    }

    @Override
    public ESnackTypes geteType() {
        return eType;
    }

    @Override
    public Double getdPrice() {
        return dPrice;
    }    
}
