/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author home
 */
public class Eugenia implements ISnack {
    final long          nId         = 4;
    final String        szName      = "Eugenia";
    final ESnackTypes   eType       = ESnackTypes.E_EUGENIA;
    final int           nInitialNo  = 10; 

    Double              dPrice      = 0.5;
    int                 nQuantity   = 10;

    private Eugenia() {}
    
    public static Eugenia getInstance() {
        return EugeniaHolder.INSTANCE;
    }
    
    private static class EugeniaHolder {
        private static final Eugenia INSTANCE = new Eugenia();
    }
    
    @Override
    public int getSoldQuantity() {
        return (nInitialNo - nQuantity);
    }

    @Override
    public int getnQuantity() {
        return nQuantity;
    }

    @Override
    public void setnQuantity(int nQuantity) {
        this.nQuantity = nQuantity;
    }
    
    @Override
    public long getnId() {
        return nId;
    }

    @Override
    public String getSzName() {
        return szName;
    }

    @Override
    public ESnackTypes geteType() {
        return eType;
    }

    @Override
    public Double getdPrice() {
        return dPrice;
    }    
}
