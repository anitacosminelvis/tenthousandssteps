/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author home
 */
public class Crisps implements ISnack {
    final long          nId         = 1;
    final String        szName      = "Crisps";
    final ESnackTypes   eType       = ESnackTypes.E_CRISPS;
    final int           nInitialNo  = 10; 
    
    Double              dPrice      = 0.75;
    int                 nQuantity   = 10;

    private Crisps() {}
    
    public static Crisps getInstance() {
        return CrispsHolder.INSTANCE;
    }
    
    private static class CrispsHolder {
        private static final Crisps INSTANCE = new Crisps();
    }
    
    @Override
    public int getSoldQuantity() {
        return (nInitialNo - nQuantity);
    }

    @Override
    public int getnQuantity() {
        return nQuantity;
    }

    @Override
    public void setnQuantity(int nQuantity) {
        this.nQuantity = nQuantity;
    }
    
    @Override
    public long getnId() {
        return nId;
    }

    @Override
    public String getSzName() {
        return szName;
    }

    @Override
    public ESnackTypes geteType() {
        return eType;
    }

    @Override
    public Double getdPrice() {
        return dPrice;
    }    
}
