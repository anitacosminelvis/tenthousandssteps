/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author home
 */
public class CocaCola implements ISnack {
    final long          nId         = 3;
    final String        szName      = "Coca Cola";
    final ESnackTypes   eType       = ESnackTypes.E_COCA_COLA;
    final int           nInitialNo  = 10; 
    
    Double              dPrice      = 1.0;
    int                 nQuantity   = 10;

    private CocaCola() {}
    
    public static CocaCola getInstance() {
        return CocaColaHolder.INSTANCE;
    }
    
    private static class CocaColaHolder {
        private static final CocaCola INSTANCE = new CocaCola();
    }
     
    @Override
    public int getSoldQuantity() {
        return (nInitialNo - nQuantity);
    }
    
    @Override
    public int getnQuantity() {
        return nQuantity;
    }

    @Override
    public void setnQuantity(int nQuantity) {
        this.nQuantity = nQuantity;
    }

    @Override
    public long getnId() {
        return nId;
    }

    @Override
    public String getSzName() {
        return szName;
    }

    @Override
    public ESnackTypes geteType() {
        return eType;
    }

    @Override
    public Double getdPrice() {
        return dPrice;
    }    
}
