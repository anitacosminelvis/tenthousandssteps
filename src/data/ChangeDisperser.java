/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author home
 */
public class ChangeDisperser {
    
    HashMap<ECoinTypes, Double>     coinValuesMap       = new HashMap<>();// coin id    -   coin value
    HashMap<ECoinTypes, Integer>    changeCountMap      = new HashMap<>();// coin id    -   counter
    ArrayList<int[]>                changeCombinations  = new ArrayList<>();
    final Double                    totalInitialChange  = 27.0;
    final int                       maximumCoins        = 20;
    DecimalFormat                   decimalForm         = new DecimalFormat("#.########");

    public Double getTotalInitialChange() {
        return totalInitialChange;
    }
    
    public Double getTotalChange(){
        Double dTotal = 0.0;
        
        for(int i = 0; i < this.changeCountMap.size(); i++) {
            if (0 < changeCountMap.get(getCoinTypeByIndex(i))){
                dTotal += changeCountMap.get(getCoinTypeByIndex(i))
                            * coinValuesMap.get(getCoinTypeByIndex(i));
            }
        }   
        
        return dTotal;
    }
    
    public void initialize(){
        initializeChangeCountMap();
        initializeCoinValuesMap();
    }
    
    public void setOptimalChange(Double dValue) throws Exception{
        
        if (0 == dValue) return;
        
        int[] tempCombination = new int[coinValuesMap.size()];

        SetAllChangeCombinations(tempCombination,ECoinTypes.E_0_05, 0,dValue);
        
        if (0 < changeCombinations.size()){
            tempCombination = GetOptimalChange();
        }else{
            throw new Exception("We are sorry, but no change is available for this selection! Please cash in your money!");
        }
        
        SetOptimalChange(tempCombination);
        
        changeCombinations.clear();
    }
    
    private int[] GetOptimalChange() {
        int optimalValue = Integer.MAX_VALUE;
        int optimalIndex = 0;
        
        for (int i = 0; i < changeCombinations.size(); i++) {
            int currentFunctionValue = 0;
            int spreadValue = 0;
            for (int j = 0; j < coinValuesMap.size(); j++){
                if (0 < changeCombinations.get(i)[j]){
                    if (4 != j) currentFunctionValue += 2 * changeCombinations.get(i)[j]
                                    + (maximumCoins - changeCountMap.get(getCoinTypeByIndex(j)));
                    ++spreadValue;
                }
            }
            currentFunctionValue -= spreadValue;
            
            if (optimalValue > currentFunctionValue){
                optimalValue = currentFunctionValue;
                optimalIndex = i;
            }
        }
        
        return changeCombinations.get(optimalIndex).clone();
    }
    
    private void SetAllChangeCombinations(int[] combination,
                                            ECoinTypes type,
                                            int coinIndex,
                                            Double nRemainingTotal){
        if (!CanSupportChange(combination)) return;
        
        for(int iCoin = coinIndex; iCoin < coinValuesMap.size(); iCoin++) {
            Double nRemainingChange = nRemainingTotal - coinValuesMap.get(getCoinTypeByIndex(iCoin));
            nRemainingChange = Double.valueOf(decimalForm.format(nRemainingChange));
            
            int[] tempCombination = combination.clone();
            tempCombination[getIndexByCoinType(getCoinTypeByIndex(iCoin))]++;

            if (!CanSupportChange(tempCombination)) break;
            
            if (0 > nRemainingChange) break;
            
            if (0 == nRemainingChange){
                if (CanSupportChange(tempCombination)) changeCombinations.add(tempCombination);
                break;
            }else{
                SetAllChangeCombinations(tempCombination,
                                            getCoinTypeByIndex(iCoin), iCoin,
                                            nRemainingChange);
            }
        }
    }
    
    boolean CanSupportChange(int[] combination){
        boolean bIsValid = true;
        
        for(int i = 0; i < changeCountMap.size(); i++) {
            if (combination[i] > changeCountMap.get(getCoinTypeByIndex(i))){
                bIsValid = false;
                break;
            }
        }   

        return bIsValid;
    }
    
    void SetOptimalChange(int[] combination){        
        for(int i = 0; i < changeCountMap.size(); i++) {
            if (0 < combination[i]){
                int nValue = changeCountMap.get(getCoinTypeByIndex(i))
                            - combination[i];
                changeCountMap.put(getCoinTypeByIndex(i),nValue);
            }
        }   
    }
    
    private void initializeCoinValuesMap(){
        coinValuesMap.put(ECoinTypes.E_0_05,0.05);
        coinValuesMap.put(ECoinTypes.E_0_1,0.1);
        coinValuesMap.put(ECoinTypes.E_0_2,0.2);
        coinValuesMap.put(ECoinTypes.E_0_5,0.5);
        coinValuesMap.put(ECoinTypes.E_1_0,1.0);   
    }
    
    private void initializeChangeCountMap(){
        changeCountMap.put(ECoinTypes.E_0_05,20);
        changeCountMap.put(ECoinTypes.E_0_1,20);
        changeCountMap.put(ECoinTypes.E_0_2,20);
        changeCountMap.put(ECoinTypes.E_0_5,20);
        changeCountMap.put(ECoinTypes.E_1_0,10);   
    }
    
    public Double getCoinValueByType(ECoinTypes type){
        return coinValuesMap.get(type);
    }
        
    public int getChangeCountByType(ECoinTypes type){
        return changeCountMap.get(type);
    }
    
    public void setChangeCountByType(ECoinTypes type, int nNewValue){
        changeCountMap.put(type,nNewValue);      
    }

    private int getIndexByCoinType(ECoinTypes type){
        int index = 0;
        switch(type){
            case E_0_05:
                index = 0;
                break;
            case E_0_1:
                index = 1;
                break;
            case E_0_2:
                index = 2;
                break;
            case E_0_5:
                index = 3;
                break;
            case E_1_0:
                index = 4;
                break;
            default:
                throw new AssertionError(type.name());
        
        }
        return index;
    }
    
    private ECoinTypes getCoinTypeByIndex(int index){
        ECoinTypes type = ECoinTypes.E_0_05;
        
        switch(index){
            case 0:
                type = ECoinTypes.E_0_05;
                break;
            case 1:
                type = ECoinTypes.E_0_1;
                break;
            case 2:
                type = ECoinTypes.E_0_2;
                break;
            case 3:
                type = ECoinTypes.E_0_5;
                break;
            case 4:
                type = ECoinTypes.E_1_0;
                break;
            default:
                throw new AssertionError(type.name());
        
        }
        
        return type;
    }
}
