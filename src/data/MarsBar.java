/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author home
 */
public class MarsBar implements ISnack {
    final long          nId         = 2;
    final String        szName      = "Mars bar";
    final ESnackTypes   eType       = ESnackTypes.E_MARS;
    final int           nInitialNo  = 10; 
        
    Double              dPrice      = 0.7;
    int                 nQuantity   = 10;

    private MarsBar() {}
    
    public static MarsBar getInstance() {
        return MarsBarHolder.INSTANCE;
    }
    
    private static class MarsBarHolder {
        private static final MarsBar INSTANCE = new MarsBar();
    }
    
    @Override
    public int getSoldQuantity() {
        return (nInitialNo - nQuantity);
    }

    @Override
    public int getnQuantity() {
        return nQuantity;
    }

    @Override
    public void setnQuantity(int nQuantity) {
        this.nQuantity = nQuantity;
    }
    
    @Override
    public long getnId() {
        return nId;
    }

    @Override
    public String getSzName() {
        return szName;
    }

    @Override
    public ESnackTypes geteType() {
        return eType;
    }

    @Override
    public Double getdPrice() {
        return dPrice;
    }    
}
